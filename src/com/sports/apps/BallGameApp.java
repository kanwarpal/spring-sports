package com.sports.apps;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import com.sports.games.*;

public class BallGameApp {

	public static void main(String[] args) {
		System.out.println("Welcome to Sports APP!! \n\t\twe believe that \""+Game.motivation+"\".");
		
		startFootball();
		startTennis();
		startHockey();
	}

	public static void startHockey() {
			System.out.println("Welcome to HOCKEY STADIUM");
			
			//Load the Spring Configuration File
			ClassPathXmlApplicationContext context_newest = new ClassPathXmlApplicationContext("beans.xml");

			//Retrieve the bean from Spring IoC container
			Game hockey = context_newest.getBean("hockey", Game.class);
			
			//call methods on bean(Do, Business Logic)
				//calling native method of LawnTennis.Java
				System.out.println("RULES: "+ hockey.getRules());
				//calling method from dependency object: fed by DI.
				System.out.println("Coach says: "+ hockey.getTraining());
				//calling native method of an implementation, so did type-cast
				System.out.println("WorldCUP date: "+ ((Hockey)hockey).getWorldcup());
			
			//close the context
			context_newest.close();
	}

	public static void startTennis() {
			System.out.println("Welcome to TENNIS STADIUM");
			
			//Load the Spring Configuration File
			ClassPathXmlApplicationContext context_new = new ClassPathXmlApplicationContext("beans.xml");
			
			//Retrieve the bean from Spring IoC container
			Game tennis = context_new.getBean("tennis", Game.class);
			
			//call methods on bean(Do, Business Logic)
				//calling native method of LawnTennis.Java
				System.out.println("RULES: "+ tennis.getRules());
				//calling method from dependency object: fed by DI.
				System.out.println("Coach says: "+ tennis.getTraining());
				//calling native method of an implementation, so did type-cast
				System.out.println("WorldCUP date: "+ ((LawnTennis)tennis).getWorldcup());
						
			//close the context
			context_new.close();
	}

	public static void startFootball() {
			System.out.println("Welcome to FOOTBALL STADIUM");
			
			//Load the spring configuration file.
			ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
		
			//Retrieve bean from Spring IoC container
			Game fifa = context.getBean("football", Game.class);
			
			//call methods on bean( Do, business logic)
				//calling native method of Football.Java
				System.out.println("RULES: "+ fifa.getRules());
				//calling method from dependency object: fed by DI.
				System.out.println("Coach says: "+ fifa.getTraining());
				//calling native method of an implementation, so did type-cast
				System.out.println("WorldCUP date: "+ ((Football)fifa).getWorldcup());
				
			//close the context
			context.close();
	}
}
