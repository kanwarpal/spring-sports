package com.sports.games;

import com.sports.coach.Coach;

public class Football implements Game {

	//Declare the dependency as private field.
	private Coach coach;
	//Define a constructor, for injecting the dependency. 
	public Football(Coach footballCoach) {
			this.coach = footballCoach;
	}
	
	//DEMO: injecting literals from properties file.
	//Step1: create file in Classpath.
	//Step2: Load Properties file into Spring Config file(beans.xml)
	//Step3: Reference literals using ${name}.
		//Declare a private field to be injected as literal from properties file.
		private String worldcup;
		
		public String getWorldcup() {
			return worldcup;
		}
		
		public void setWorldcup(String worldcup) {
			this.worldcup = worldcup;
		}
	
	@Override
	public String getRules() {
		// TODO Auto-generated method stub
		return "One Ball and 2X11X2 feet";
	}

	@Override
	public String getTraining() {
		// TODO Auto-generated method stub
		return coach.getDailyPractice();
	}

}
