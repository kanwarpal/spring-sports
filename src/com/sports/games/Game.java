package com.sports.games;

public interface Game {

	String motivation = "Persistance is Key";
	
	public String getRules();
	
	public String getTraining();
	
}
