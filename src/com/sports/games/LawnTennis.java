package com.sports.games;

import com.sports.coach.Coach;

public class LawnTennis implements Game {
	
	//Declare the dependency as private field.
	private Coach coach;
	//Declare the dependency as private field.
	//Define a constructor, for injecting the dependency.
	public LawnTennis(Coach lawnTennisCoach) {
		this.coach = lawnTennisCoach;
	}
	
			//DEMO: injecting literals from properties file.
			//Step1: create file in Classpath.
			//Step2: Load Properties file into Spring Config file(beans.xml)
			//Step3: Reference literals using ${name}.
			//Declare a private field to be injected as literal from properties file.
			private String worldcup;
			
			public String getWorldcup() {
				return worldcup;
			}
			
			//Create Setter for dependency
			public void setCoach(Coach hockeyCoach) {
				this.coach = hockeyCoach;
			}
	
	@Override
	public String getRules() {
		return "Tennis Ball and Rackets, could be singles or doubles.";
	}
	
	@Override
	public String getTraining() {
		return coach.getDailyPractice();
	}

}
