package com.sports.games;

import com.sports.coach.Coach;

public class Hockey implements Game {

	//Declare dependency as private field,
	// and a setter is required(declared below).
	private Coach coach;
	//Create Setter for dependency
	public void setWorldcup(String worldcup) {
			this.worldcup = worldcup;
	}
	
		//DEMO: injecting literals from properties file.
		//Step1: create file in Classpath.
		//Step2: Load Properties file into Spring Config file(beans.xml)
		//Step3: Reference literals using ${name}.
		//Declare a private field to be injected as literal from properties file.
		private String worldcup;
		
		public String getWorldcup() {
			return worldcup;
		}
		
		//Create Setter for dependency
		public void setCoach(Coach hockeyCoach) {
			this.coach = hockeyCoach;
		}
		
	@Override
	public String getRules() {
		return "roll it using a hockey stick";
	}

	@Override
	public String getTraining() {
		return coach.getDailyPractice();
	}

}
